package spring1.springDemo.web;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/movie")
@RestController
public class MovieController {
    List<Movie> movielist = new ArrayList<>();




    @PostMapping
    public String addMovie(@RequestBody Movie movie){
        movielist.add(movie);
        return "JEST dodany film";
    }
///hhhhhhhhhh
//wyszukiwanie po parametrze
/////http://localhost:8080/movie?title=nic

    @GetMapping
    public List<Movie> lista(@RequestParam(required = false) String title){
        return movielist.stream()
                .filter(movie -> title==null || movie.getTitle().contains(title))
                ///movie.getTitle().equals(title) - ----> dokładnie pokazuje po nazwie
                .collect(Collectors.toList());
    }


//stara metoda
//     @GetMapping
//    public List<Movie> lista(){
//        return movielist;
//     }

}
