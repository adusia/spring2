package spring1.springDemo.web;

public class Movie {

    private int id;
    private String Title;
    private int Year;
    private String Type;

    public Movie() {
    }

    public Movie(int id, String title, int year, String type) {
        this.id = id;
        Title = title;
        Year = year;
        Type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
