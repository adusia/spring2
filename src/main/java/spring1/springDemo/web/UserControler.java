package spring1.springDemo.web;

import org.springframework.web.bind.annotation.*;

//@RequestMapping("/user")
//@RestController
///public class UserControler {
//    User user1 = new User("Ada","Karpow");
//
//    @RequestMapping(value="/name",method = RequestMethod.GET)
//    public String pobierzImie(){
//        return user1.getName();
//    }

    @RequestMapping("/user")
    @RestController
    class UserController {

        @GetMapping
        public User getUser(){
            return new User("Ada2","Karpow2");
        }

        //curl -X POST -H "Content-type:application:json" -d '{"name":"Piotr', "lastName":"Brzozowski"}' localhost:8080/user

        @PostMapping
        public User addUser(@RequestBody User user){
            user.setLastname(user.getName());
            return user;
        }

//        @RequestMapping(method = RequestMethod.GET)
//        public String getFullName(){
//            return "Piotr Brzozowski";
//        }
//
//        @RequestMapping(value = "/name",
//                method = RequestMethod.GET)
//        public String getName() {
//            return "Piotr";
//        }
//
//        @RequestMapping(value = "/lastname", method = RequestMethod.GET)
//        public String lastName() {
//            return "Brzozowski";
//
//        }
    }




