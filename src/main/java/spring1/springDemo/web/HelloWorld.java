package spring1.springDemo.web;

import org.springframework.web.bind.annotation.*;

@RequestMapping("/hello2")
@RestController
public class HelloWorld {

    @GetMapping("/test")
    public String helloworld(){
        return "witajcie w springu";
    }

    @GetMapping
    public String hellowarord(){
        return "Pierwsza metoda";
    }
///curl -X POST localhost:8080/hello2
    @PostMapping
    public String addHello(){
        return "Tetujemy metode post moga byc tylko get i post alke tylko na hednym ednpoincie";
    }
}
